This data represents the crop research done by [Monfreda *et al.*](http://www.earthstat.org/harvested-area-yield-175-crops/), cross-referenced with [Koppen climate data](https://en.wikipedia.org/wiki/K%C3%B6ppen_climate_classification).

## How the data was generated

The Python package `skimage` was used to load the crop yield images from Monfreda *et al.* for each of the 175 researched crops.
Refer to the original work for a detailed description of each item.
The number of cells containing each item was compared to the total land area of each Koppen classification.

Aggregation was performed using a weighted function based on the measured yield of each crop relative to the maximum.
Absolute yield was not considered.
For example, a crop in an `Af` climate with 10 pixels of 100% maximum yield and 5 pixels of 50% yield was calculated as: 10 * 1.0 + 5 * 0.5 = 12.5 total weighted pixels.

This number was then divided by the total pixels in that Koppen classification. For example, for the above crop, if the total area of `Af` climate is 100, then the crop is cultivated in 12.5% of that climate.

## How to use the data

The final collection of numbers gives an approximate idea of where different crops appear on earth relative to Koppen climates. Lentiles, for example, are farmed in approximately 57.7% of `Dwa` climates.

### Disclaimers

* This data has not been double-checked with reference to the climatic or environmental needs of each crop. For instance, many crops are listed as prevalent in `ET` climates due to local cultivation practices but this may not be applicable to all `ET` areas.
* This data does not take regional, cultural, or historical influences into account
* Please also reference the FAQ from the original data

## References

1. Monfreda, C., N. Ramankutty, and J. A. Foley (2008), Farming the planet: 2. Geographic distribution of crop areas, yields, physiological types, and net primary production in the year 2000, *Global Biogeochem. Cycles*, 22, GB1022, doi: [10.1029/2007GB002947](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2007GB002947).